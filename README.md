# srrg_boss

This package contains the Basic Object Serialization System,
a compact library to serialize and deserialize complex linked structures
such as graphs, in a text file.

The most complex SRRG packages, for instance the map rely on BOSS for the serialization/deserialization.

## Getting Started

## Authors

* Daniele Baldassari  
* Giorgio Grisetti 

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

BSD 2.0
